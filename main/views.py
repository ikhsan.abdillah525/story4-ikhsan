from django.shortcuts import render


def home(request):
    return render(request, 'main/about.html')

def about(request):
    return render(request, 'main/about.html')

def gallery(request):
    return render(request, 'main/gallery.html')

def exp(request):
    return render(request, 'main/exp.html')

def contact(request):
    return render(request, 'main/contact.html')

def meetme(request):
    return render(request, 'main/hi.html')