from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.about, name='home'),
    path('about', views.about, name='about'),
    path('gallery', views.gallery, name='gallery'),
    path('experience', views.exp, name='exp'),
    path('contact', views.contact, name='contact'),
    path('meetme', views.meetme, name='meetme'),

]
