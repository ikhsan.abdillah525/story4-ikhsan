from django.urls import path

from . import views

app_name = 'story1'

urlpatterns = [
    path('home', views.home, name='home'),
]
